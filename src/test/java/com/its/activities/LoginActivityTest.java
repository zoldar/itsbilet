package com.its.activities;

import com.its.R;
import com.its.activities.LoginActivity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(RobolectricTestRunner.class)
public class LoginActivityTest {
    @Test
    public void shouldHaveHappySmiles() throws Exception {
        String appName = new LoginActivity().getResources().getString(R.string.app_name);
        assertThat(appName, equalTo("ITS Bilet"));
    }
}
