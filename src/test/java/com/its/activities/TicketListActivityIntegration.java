package com.its.activities;

import com.google.mockwebserver.MockResponse;
import com.google.mockwebserver.MockWebServer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class TicketListActivityIntegration {
    private MockWebServer server;

    @Before
    public void setupWebServer() {
        server = new MockWebServer();
        server.enqueue(new MockResponse().setBody("[{\"id\": 1, \"name\": \"Bilet 1\"}, {\"id\": 2, \"name\": \"Bilet2\"}]"));
    }

    @Test
    public void shouldRenderListFromRESTResource() {


    }
}
