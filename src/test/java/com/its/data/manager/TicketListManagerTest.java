package com.its.data.manager;

import com.its.data.model.TicketList;
import com.its.data.utils.DataManagerException;
import com.its.data.utils.DataManagerRequestCallback;
import com.its.settings.ItsConfiguration;
import com.its.utils.MockedSpiceManager;
import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import org.apache.http.message.BasicHeader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@RunWith(RobolectricTestRunner.class)
public class TicketListManagerTest {
    TicketList returnedTicketList;

    @Test
    public void shouldReturnListOfAllTickets() throws Exception {
        ItsConfiguration config = new ItsConfiguration();

        Robolectric.addPendingHttpResponse(
                200,
                "{\"tickets\": [{\"id\": 1, \"name\": \"Ticket 1\"}, {\"id\": 2, \"name\": \"Ticket 2\"}]}",
                new BasicHeader("Content-Type", "application/json"));

        TicketListManager manager = new TicketListManager(
                new MockedSpiceManager(JacksonSpringAndroidSpiceService.class),
                config,
                Robolectric.application);

        manager.getAllTickets(new DataManagerRequestCallback() {
            @Override
            public void onFailure(DataManagerException e) {
                throw e;
            }

            @Override
            public void onSuccess(TicketList ticketList) {
                returnedTicketList = ticketList;
            }
        });

        assertNotNull(returnedTicketList);
        assertThat(returnedTicketList.getTickets().size(), equalTo(2));
    }
}
