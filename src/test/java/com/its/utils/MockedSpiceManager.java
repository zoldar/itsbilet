package com.its.utils;

import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.SpiceService;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class MockedSpiceManager extends SpiceManager {
    private JacksonSpringAndroidSpiceService spiceService;

    public MockedSpiceManager(Class<? extends SpiceService> spiceServiceClass) {
        super(spiceServiceClass);
        try {
            spiceService = (JacksonSpringAndroidSpiceService) spiceServiceClass.newInstance();
            spiceService.createRestTemplate();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public <T> void execute(final SpiceRequest<T> request, final RequestListener<T> requestListener) {
        SpringAndroidSpiceRequest<T> realRequest = (SpringAndroidSpiceRequest<T>) request;
        realRequest.setRestTemplate(spiceService.createRestTemplate());
        try {
            requestListener.onRequestSuccess(realRequest.loadDataFromNetwork());
        } catch (Exception e) {
            if (e instanceof SpiceException) {
                requestListener.onRequestFailure((SpiceException) e);
            } else {
                requestListener.onRequestFailure(new SpiceException(
                        "Something bad happened", e));
            }
        }
    }
}
