package com.its.settings;

import com.google.common.collect.Maps;

import java.util.HashMap;
import java.util.Map;

public class ItsConfiguration {
    private final Map<String, String> configuration;

    private final Map<String, String> defaultConfiguration = new HashMap<String, String>() {{
        put("rest.base_url", "http://localhost/its-v1");
        put("rest.url.ticket_list", "/tickets");
    }};

    public ItsConfiguration() {
        this(new HashMap<String, String>());
    }

    public ItsConfiguration(Map<String, String> configuration) {
        if (!configuration.isEmpty()) {
            this.configuration = configuration;
        } else {
            this.configuration = defaultConfiguration;
        }
    }

    public String get(String parameter) {
        return configuration.get(parameter);
    }
}
