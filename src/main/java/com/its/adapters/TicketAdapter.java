package com.its.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.its.R;
import com.its.data.model.Ticket;

import java.util.List;

public class TicketAdapter  extends BaseAdapter {
    private List<Ticket> tickets;
    private final Context context;

    public TicketAdapter(Context context, List<Ticket> tickets) {
        this.context = context;
        this.tickets = tickets;
    }

    @Override
    public int getCount() {
        return tickets.size();
    }

    @Override
    public Ticket getItem(int position) {
        return tickets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return tickets.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Ticket ticket = getItem(position);
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.ticketlist_row, parent, false);
            holder = new ViewHolder();
            holder.id = (TextView) convertView.findViewById(R.id.ticketlist_row_id);
            holder.name = (TextView) convertView.findViewById(R.id.ticketlist_row_name);
            holder.viewButton = (Button) convertView.findViewById(R.id.ticketlist_view_button);
            holder.shareButton = (Button) convertView.findViewById(R.id.ticketlist_share_button);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.id.setText(Long.toString(ticket.getId()));
        holder.name.setText(ticket.getName());

        return convertView;
    }

    static class ViewHolder {
        TextView id;
        TextView name;
        Button viewButton;
        Button shareButton;
    }
}
