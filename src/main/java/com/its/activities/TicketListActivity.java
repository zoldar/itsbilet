package com.its.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.fortysevendeg.android.swipelistview.SwipeListView;
import com.google.common.collect.Lists;
import com.googlecode.androidannotations.annotations.EActivity;
import com.its.R;
import com.its.adapters.TicketAdapter;
import com.its.data.manager.TicketListManager;
import com.its.data.model.Ticket;
import com.its.data.model.TicketList;
import com.its.data.utils.DataManagerException;
import com.its.data.utils.DataManagerRequestCallback;
import com.its.settings.ItsConfiguration;
import com.octo.android.robospice.Jackson2SpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;

import java.util.List;

@EActivity
public class TicketListActivity extends SherlockFragmentActivity {
    private List<Ticket> tickets;
    private TicketAdapter adapter;
    private SwipeListView ticketListView;
    private TicketListManager manager;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.ticketlist_activity_view);

        tickets = Lists.newArrayList();
        manager = new TicketListManager(
                new SpiceManager(Jackson2SpringAndroidSpiceService.class),
                new ItsConfiguration(),
                this);
        adapter = new TicketAdapter(this, tickets);

        ticketListView = (SwipeListView) findViewById(R.id.ticket_list);
        ticketListView.setAdapter(adapter);

        manager.getAllTickets(new DataManagerRequestCallback() {
            @Override
            public void onFailure(DataManagerException e) {
                CharSequence failureMsg = "Failed to fetch tickets";
                Toast toast = Toast.makeText(getApplicationContext(),
                        failureMsg, Toast.LENGTH_SHORT);
                dismissProgressDialog();
                toast.show();
            }

            @Override
            public void onSuccess(TicketList ticketList) {
                dismissProgressDialog();
                tickets = ticketList.getTickets();
                adapter.notifyDataSetChanged();
            }

            private void dismissProgressDialog() {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu; this adds items to the action bar if it is present.
	    getSupportMenuInflater().inflate(com.its.R.menu.main, menu);
	    return true;
    }
}