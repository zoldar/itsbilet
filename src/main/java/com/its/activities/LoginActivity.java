package com.its.activities;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.googlecode.androidannotations.annotations.EActivity;
import com.its.R;

@EActivity(R.layout.activity_main)
public class LoginActivity extends SherlockFragmentActivity {
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	getSupportMenuInflater().inflate(com.its.R.menu.main, menu);
	return true;
    }

}

