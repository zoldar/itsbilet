package com.its.data.rest;

import android.net.Uri;
import com.its.data.model.TicketList;
import com.its.settings.ItsConfiguration;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

public class TicketListRestRequest extends SpringAndroidSpiceRequest<TicketList> {
    private static final int WEBSERVICES_TIMEOUT = 5;

    private final ItsConfiguration config;

    public TicketListRestRequest(ItsConfiguration config) {
        super(TicketList.class);
        this.config = config;
    }

    @Override
    public TicketList loadDataFromNetwork() throws Exception {
        Uri.Builder uriBuilder = Uri.parse(config.get("rest.base_url")).buildUpon();
        uriBuilder.appendPath(config.get("rest.url.ticket_list"));
        String url = uriBuilder.build().toString();
        manageTimeOuts(getRestTemplate());
        return getRestTemplate().getForObject(url, TicketList.class);
    }

    private void manageTimeOuts(RestTemplate restTemplate) {
        // set timeout for requests
        ClientHttpRequestFactory factory = restTemplate.getRequestFactory();
        if (factory instanceof HttpComponentsClientHttpRequestFactory) {
            HttpComponentsClientHttpRequestFactory advancedFactory = (HttpComponentsClientHttpRequestFactory) factory;
            advancedFactory.setConnectTimeout(WEBSERVICES_TIMEOUT);
            advancedFactory.setReadTimeout(WEBSERVICES_TIMEOUT);
        } else if (factory instanceof SimpleClientHttpRequestFactory) {
            SimpleClientHttpRequestFactory advancedFactory = (SimpleClientHttpRequestFactory) factory;
            advancedFactory.setConnectTimeout(WEBSERVICES_TIMEOUT);
            advancedFactory.setReadTimeout(WEBSERVICES_TIMEOUT);
        }
    }
}
