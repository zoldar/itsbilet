package com.its.data.manager;

import android.content.Context;
import com.its.R;
import com.its.data.model.TicketList;
import com.its.data.rest.TicketListRestRequest;
import com.its.data.utils.DataManagerException;
import com.its.data.utils.DataManagerRequestCallback;
import com.its.settings.ItsConfiguration;
import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

public class TicketListManager {
    private final SpiceManager contentManager;
    private final Context context;
    private final ItsConfiguration config;

    public TicketListManager(SpiceManager contentManager, ItsConfiguration config, Context context) {
        this.contentManager = contentManager;
        this.context = context;
        this.config = config;
    }

    public void getAllTickets(DataManagerRequestCallback callback) {
        TicketListRestRequest request = new TicketListRestRequest(config);
        contentManager.execute(request, new TicketListRequestListener(callback));
    }

    private class TicketListRequestListener implements RequestListener<TicketList> {
        private DataManagerRequestCallback callback;

        public TicketListRequestListener(DataManagerRequestCallback callback) {
            this.callback = callback;
        }

        @Override
        public void onRequestFailure(SpiceException e) {
            callback.onFailure(new DataManagerException(
                    context.getString(R.string.data_manager_network_error), e));
        }

        @Override
        public void onRequestSuccess(TicketList ticketList) {
            callback.onSuccess(ticketList);
        }
    }
}
