package com.its.data.utils;

import com.its.data.model.TicketList;

public interface DataManagerRequestCallback {
    void onFailure(DataManagerException e);
    void onSuccess(TicketList ticketList);
}
