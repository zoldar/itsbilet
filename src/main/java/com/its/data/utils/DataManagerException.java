package com.its.data.utils;

public class DataManagerException extends RuntimeException {
    public DataManagerException(String message, Throwable e) {
        super(message, e);
    }
}
